CREATE DATABASE  IF NOT EXISTS `todolist` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `todolist`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: todolist
-- ------------------------------------------------------
-- Server version	5.5.24-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `td_lists`
--

DROP TABLE IF EXISTS `td_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `td_lists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `ow` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  `d_created` int(10) unsigned NOT NULL DEFAULT '0',
  `d_edited` int(10) unsigned NOT NULL DEFAULT '0',
  `sorting` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `taskview` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `td_lists`
--

LOCK TABLES `td_lists` WRITE;
/*!40000 ALTER TABLE `td_lists` DISABLE KEYS */;
INSERT INTO `td_lists` VALUES (1,'d15d7f8a-9189-4cd8-927f-63cf8277c69e',0,'Tareas Club',1375887634,1375887665,0,0,0),(2,'3e554b29-0e8f-4483-8204-b7a4e36ba512',1,'Tareas Personales',1375887670,1375887670,0,0,0),(3,'3f58d824-8ba4-409a-ae66-fb3312a2eed4',2,'Tareas AVVOCATI',1375887677,1375887677,0,0,0);
/*!40000 ALTER TABLE `td_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `td_tag2task`
--

DROP TABLE IF EXISTS `td_tag2task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `td_tag2task` (
  `tag_id` int(10) unsigned NOT NULL,
  `task_id` int(10) unsigned NOT NULL,
  `list_id` int(10) unsigned NOT NULL,
  KEY `tag_id` (`tag_id`),
  KEY `task_id` (`task_id`),
  KEY `list_id` (`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `td_tag2task`
--

LOCK TABLES `td_tag2task` WRITE;
/*!40000 ALTER TABLE `td_tag2task` DISABLE KEYS */;
INSERT INTO `td_tag2task` VALUES (1,2,1),(2,3,1),(4,5,3),(5,6,1),(6,7,2),(3,4,3),(8,9,3),(8,8,3),(9,10,1),(5,11,1),(3,12,3),(3,13,3),(10,13,3),(11,14,3),(4,16,3),(3,17,3),(10,17,3),(13,18,3),(8,15,3),(3,19,3),(3,20,3),(3,21,3),(10,21,3),(10,22,3),(10,23,3),(14,24,1);
/*!40000 ALTER TABLE `td_tag2task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `td_tags`
--

DROP TABLE IF EXISTS `td_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `td_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `td_tags`
--

LOCK TABLES `td_tags` WRITE;
/*!40000 ALTER TABLE `td_tags` DISABLE KEYS */;
INSERT INTO `td_tags` VALUES (7,'Busqueda'),(13,'Catalogos'),(11,'Cliente'),(4,'Clientes'),(14,'Comscore'),(6,'Cotización'),(3,'Demandas'),(5,'Eventos'),(12,'Extrajudicial'),(10,'Extrajudiciales'),(8,'General'),(2,'Menu'),(1,'Promociones'),(9,'Suscripciones');
/*!40000 ALTER TABLE `td_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `td_todolist`
--

DROP TABLE IF EXISTS `td_todolist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `td_todolist` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) NOT NULL DEFAULT '',
  `list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `d_created` int(10) unsigned NOT NULL DEFAULT '0',
  `d_completed` int(10) unsigned NOT NULL DEFAULT '0',
  `d_edited` int(10) unsigned NOT NULL DEFAULT '0',
  `compl` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL,
  `note` text,
  `prio` tinyint(4) NOT NULL DEFAULT '0',
  `ow` int(11) NOT NULL DEFAULT '0',
  `tags` varchar(600) NOT NULL DEFAULT '',
  `tags_ids` varchar(250) NOT NULL DEFAULT '',
  `duedate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `list_id` (`list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `td_todolist`
--

LOCK TABLES `td_todolist` WRITE;
/*!40000 ALTER TABLE `td_todolist` DISABLE KEYS */;
INSERT INTO `td_todolist` VALUES (1,'7ee2d3f0-3fa4-4300-88a5-c6d2ad8fc2ac',1,1375888046,1375888057,1375888057,1,'Registro de lectores en promociones',NULL,0,1,'','',NULL),(2,'e19b48d7-c1ee-48a0-bf5d-e3efd7c45217',1,1375888144,1375897704,1375897704,1,'Registro de lectores desde promociones','Se tendrá el mecanismo para que los lectores se registren en las promociones y poder aumentar la Base de Datos',2,2,'Promociones','1','2013-08-07'),(3,'e1af6873-00d6-4983-91da-ee9e312f8b21',1,1375888246,1376087743,1376087743,1,'Menu Multinivel','Terminar la configuración del menu multinivel',-1,5,'Menu','2','2013-08-16'),(4,'3d2c6236-1eb3-435b-aff5-7213461b9d56',3,1375888490,1376085038,1376085038,1,'Seguimiento de la demanda (AFINAR)','Afinar la información que aparece en el seguimiento de la demanda',1,1,'Demandas','3','2013-08-08'),(5,'0ff62050-5021-4159-b793-0ba4146bbb84',3,1375888523,0,1376089877,0,'Crear usuario de clientes','Crear el usuario del cliente cuando este es creado',0,4,'Clientes','4','2013-08-07'),(6,'39990fd5-2dcf-4e6f-8ab9-0babb556c0b7',1,1375890296,0,1376089863,0,'Trailers para Premiers','Tener la posibilidad de agregar video de trailer a la descripción de las premiers',-1,4,'Eventos','5',NULL),(7,'5cea0204-2269-492f-af9e-61d457ab64c8',2,1375892013,0,1375892013,0,'Cotizar Hosting para AVVOCATI','Buscar propuestas de hosting para AVVOCATI',-1,1,'Cotización','6','2013-08-17'),(8,'36f49a48-5995-43dd-9a8a-8085abb816b3',3,1375904259,0,1376089877,0,'Busqueda','Realizar la búsqueda avanzada de los diferentes rubros del AVVOCATI',0,5,'General','8','2013-08-07'),(9,'c2ad08dd-1499-47a0-9d83-8a830f443d67',3,1375919066,0,1376089881,0,'Dashboard por perfil','Crear el Dashboard por perfil',2,16,'General','8','2013-08-17'),(10,'6d7de295-a242-48f3-b744-e2c34929ea88',1,1375982239,1375986932,1375986932,1,'Suscripciones Visual','Hacer mas visual la parte de suscripciones',2,3,'Suscripciones','9','2013-08-08'),(11,'afa768fa-8e92-4ed8-8a76-6f89cf308e22',1,1375982270,1375990438,1375990438,1,'Eventos pasados','Mantener eventos pasados y etiquetarlos',2,4,'Eventos','5','2013-08-08'),(12,'909b66fd-d071-441c-b264-84f9611cdd3f',3,1376059001,1376086017,1376086017,1,'Clave estado de la demanda','Quitar la clave de la demanda y poner el nombre del estado',-1,2,'Demandas','3','2013-08-12'),(13,'23016436-f69f-4c49-9e2b-0a0206937773',3,1376059051,0,1376089881,0,'Filtros avanzados','Filtros de búsquedas avanzados en demandas y extrajudiciales',1,7,'Demandas,Extrajudiciales','3,10','2013-08-12'),(14,'54f971fc-d72c-495f-ad3d-27dedaf44295',3,1376059127,0,1376089888,0,'Información cliente','Definir información que puede ver el cliente',2,15,'Cliente','11','2013-08-12'),(15,'7e24f531-6f97-4628-a81c-c67d8258cd57',3,1376059203,1376086573,1376086573,1,'Etiquetas','Registrar tipo de Juicio -> Agregar tipo de Juicio\nRegistrar tipo de actividad -> Agregar eventos judiciales\nEtapas -> Etapas Procesales\nFolio -> Expediente',-1,3,'General','8','2013-08-12'),(16,'9ccc77cd-0d14-4d5f-94fc-704090ae155d',3,1376059253,0,1376089874,0,'Información del cliente','Agregar domicilio, telefonos por tipo y con historial, croquis domicilio',-1,2,'Clientes','4','2013-08-12'),(17,'3d45bc81-74be-43e1-9d41-df0215e8b345',3,1376059302,0,1376089877,0,'Cantidad','No siempre se pelea una cantidad, cambiar el campo para que sea alfanumerico',-1,3,'Demandas,Extrajudiciales','3,10','2013-08-12'),(18,'c3d6b49d-aa98-41da-9f86-27414832a34d',3,1376059345,1376087454,1376087454,1,'Catalogo de Entidades','Crear catalogo de entidades',2,4,'Catalogos','13','2013-08-12'),(19,'031a96d5-5b74-4408-a76c-0e69060fb76e',3,1376059401,1376087645,1376087645,1,'Dias del termino','Los dias del termino es un dato obligatorio',2,5,'Demandas','3','2013-08-12'),(20,'a3b4f42e-e71f-4539-919a-0802145a99d2',3,1376059431,0,1376089888,0,'Semaforo','Saber si una demanda es prioritaria',1,11,'Demandas','3','2013-08-12'),(21,'0448d06e-6f25-4453-ae87-6196cea53e40',3,1376059467,0,1376089888,0,'Registrar visitas','Registar la ultima visita por usuario a una demanda o extrajudicial',2,14,'Demandas,Extrajudiciales','3,10','2013-08-12'),(22,'5629773b-a4a1-4b9b-b2a2-9dc0affb9b8f',3,1376059506,0,1376089888,0,'Convenio','Campo en edición para saber si un extrajudicial está en convenio',1,12,'Extrajudiciales','10','2013-08-12'),(23,'d7fa11f9-ffde-45b7-81e8-1b57463dd250',3,1376059534,0,1376089888,0,'Finalizar extrajudiciales','Tener la posibilidad de matar extrajudiciales',1,13,'Extrajudiciales','10','2013-08-12'),(24,'4ccda3f2-68c3-494e-85f5-1f1867d3abac',1,1376059558,0,1376089863,0,'VALIDAR Comscore','VALIDAR Configurar correctamente el ComScore',2,3,'Comscore','14','2013-08-09');
/*!40000 ALTER TABLE `td_todolist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'todolist'
--

--
-- Dumping routines for database 'todolist'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-08-09 19:03:21
