<?php
$config = array();
$config['db'] = 'mysql';
$config['mysql.host'] = 'localhost';
$config['mysql.db'] = 'todolist';
$config['mysql.user'] = 'root';
$config['mysql.password'] = '';
$config['prefix'] = 'td_';
$config['url'] = '';
$config['mtt_url'] = '';
$config['title'] = '';
$config['lang'] = 'es-mx';
$config['password'] = '3010es1frp';
$config['smartsyntax'] = 1;
$config['timezone'] = 'America/Mexico_City';
$config['autotag'] = 1;
$config['duedateformat'] = 1;
$config['firstdayofweek'] = 1;
$config['session'] = 'default';
$config['clock'] = 24;
$config['dateformat'] = 'j M Y';
$config['dateformat2'] = 'n/j/y';
$config['dateformatshort'] = 'j M';
$config['template'] = 'default';
$config['showdate'] = 1;
?>